package com.mavha.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mavha.dao.PersonRepository;
import com.mavha.model.Person;

@Service
public class PersonService {

	@Autowired
	private PersonRepository personRepository;

	public List<Person> getAllPersons() {
		return personRepository.findAll();
	}

	public List<Person> getPersonsByFullNameAndAge(String firstName, String lastName, int age) {
		return personRepository.findByFullNameAndAge(firstName, lastName, age);
	}

	@Transactional
	public void savePerson(Person person) {
		personRepository.save(person);
	}
}
