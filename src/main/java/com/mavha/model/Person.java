package com.mavha.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Person {

	public Person() {}

	public Person(String dni, String firstName, String lastName, int age) {
		super();
		this.dni = dni;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	@Id
	private String dni;

	private String firstName;
	private String lastName;
	private int age;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
}
