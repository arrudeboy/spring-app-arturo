package com.mavha.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mavha.model.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

	@Query("select p from Person p where p.firstName = :firstName and p.lastName = :lastName and p.age = :age")
	public List<Person> findByFullNameAndAge(@Param("firstName") String firstName, @Param("lastName") String lastName,
			@Param("age") int age);

}
