package com.mavha.controller;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.CoreMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mavha.model.Person;
import com.mavha.service.PersonService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PersonController.class)
public class PersonControllerTest {

	static final ObjectMapper JSON_MAPPER = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PersonService personService;

	Person mockPerson = new Person("34.589.588", "Arturo", "Chari", 29);
	Person mockPerson2 = new Person("11.111.111", "Lorem", "Ipsum", 1000);
	List<Person> allPersons = Arrays.asList(mockPerson, mockPerson2);
	List<Person> somePersons = Arrays.asList(mockPerson);

	@Test
	public void retrieveAllPersons() throws Exception {
		given(personService.getAllPersons()).willReturn(allPersons);

		mockMvc.perform(get("/all").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(2)));
	}

	@Test
	public void retrievePersons() throws Exception {
		given(personService.getPersonsByFullNameAndAge(mockPerson.getFirstName(), mockPerson.getLastName(),
				mockPerson.getAge())).willReturn(somePersons);

		mockMvc.perform(get("/find?firstName=Arturo&lastName=Chari&age=29").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].firstName", is("Arturo")))
				.andExpect(jsonPath("$[0].lastName", is("Chari")))
				.andExpect(jsonPath("$[0].age", is(29)));
	}

	@Test
	public void createPerson() throws Exception {
		Person newMockPerson = new Person("12.345.678", "Juan", "Perez", 34);

		mockMvc.perform(post("/save").contentType(MediaType.APPLICATION_JSON)
				.content(JSON_MAPPER.writeValueAsString(newMockPerson)))
				.andExpect(status().isCreated());
	}
}
